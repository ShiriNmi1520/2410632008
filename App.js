import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, Button, Alert, TextInput } from "react-native";

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {text: ''}
  }
  render(){
    return (
    <View style={styles.container}>
      <Text>yee</Text>
      <StatusBar style="auto" />
      <TextInput style={{height:40, borderColor:'gray', borderWidth:1}}
        onChangeText={(text) => this.setState({text})}
        value={this.state.text}
      />
      <Button
        onPress={() => {console.log('asd');console.log(this.state.text)}}
        title="GET INFO"
        color="#841584"
        accessibilityLabel="Learn more about this purple button"
      />
    </View>
  );
  }
  }
  
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
